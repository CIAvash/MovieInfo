![Screenshot of a title](screenshots/MovieInfo-title.jpg)

[MovieInfo](https://ciavash.gitlab.io/MovieInfo/)
=========

A web app for viewing movie information and ratings.

Description
===========

MovieInfo makes it easy for you to find information and ratings for movies, TV series and games.

It shows ratings from IMDB, Rotten Tomatoes and Metacritic.
It also shows an overall rating, which is the average of the ratings from the aforementioned websites.

[OMDb API](https://omdbapi.com/) is used for obtaining movie data.

Repository
==========

<https://gitlab.com/CIAvash/MovieInfo>

Bugs
====

<https://gitlab.com/CIAvash/MovieInfo/issues>

Author
======

Siavash Askari Nasr - <http://ciavash.name/>

Copyright and License
=====================
Copyright (C) 2016 Siavash Askari Nasr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.